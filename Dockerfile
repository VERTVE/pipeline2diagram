FROM debian:10-slim

RUN apt update && apt install -y \
	python3 \
	python3-pip \
	python3-pygraphviz \
	git \
        && adduser --shell /sbin/nologin --disabled-login --gecos "" appuser

RUN pip3 install pyyaml 

ADD pipeline2diagram.py /pipeline2diagram/

USER appuser

WORKDIR /home/appuser

ENTRYPOINT ["/usr/bin/python3","/pipeline2diagram/pipeline2diagram.py"]
CMD ["-h"]
