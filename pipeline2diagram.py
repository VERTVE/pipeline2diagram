import sys
import argparse
import yaml

try:
	import pygraphviz as pgv
except ImportError:
	print("Pygraphviz is not installed, using stdout only")


class Pipeline2Diagram:
	def __init__(self):
		pass

	# Parse passed arguments
	@staticmethod
	def parse_arguments():
		parser = argparse.ArgumentParser(description='Visualize Concourse pipeline yaml files.')
		parser.add_argument('-p', '--pipeline', required=False, help="pipeline yaml to load")
		parser.add_argument('-d', '--dot', required=False, help=".dot file name")
		parser.add_argument('-g', '--png', required=False, help=".png file name")
		return parser.parse_args()

	# Read yaml file
	@staticmethod
	def read_yaml(args):
		if not args.pipeline:
			pipeline_name = 'pipeline.yml'
		else:
			pipeline_name = args.pipeline
		try:
			print("Pipeline:", pipeline_name, "\n")
			with open(pipeline_name, 'r') as pipeline_file:
				data = yaml.safe_load(pipeline_file)
				return data
		except IOError:
			print("File \"{}\" not found".format(pipeline_name))
			sys.exit(1)

	# Function to parse through dicts/lists from the yaml, searching keywords
	@staticmethod
	def search_key(to_search, keyword, i, pipe_flow):
		if isinstance(to_search, dict):
			for key, value in to_search.items():
				if key == keyword:
					pipe_flow.append(value)
					return pipe_flow
				Pipeline2Diagram.search_key(value, keyword, i, pipe_flow)
		elif isinstance(to_search, list):
			i = 0
			for key in to_search:
				i += 1
				Pipeline2Diagram.search_key(key, keyword, i, pipe_flow)
		return pipe_flow

	# Create "jobs" dict from yaml
	@staticmethod
	def get_plan(data, key_to_find):
		jobs = {}
		for job_nr in range(0, len(data['jobs'])):
			job_name = Pipeline2Diagram.search_key(data['jobs'][job_nr], 'name', job_nr, [])
			job_passed = Pipeline2Diagram.search_key(data['jobs'][job_nr], key_to_find, job_nr, [])
			jobs[job_nr] = [{'name': job_name}, {key_to_find: job_passed}]
		return jobs

	# Arrange jobs according to "passed" attribute
	@staticmethod
	def arrange_stages(job, jobs, flow):
		for stage_number in jobs:
			job_passed = str(jobs[stage_number][1]['passed']).strip("[]'")
			if job in job_passed:
				next_job = str(jobs[stage_number][0]['name']).strip("[]'")
				flow.append([job, next_job])


	# Make a list of "job, next job" -lists
	@staticmethod
	def list_jobs(jobs, flow):
		for job_number in jobs:
			job = str(jobs[job_number][0]['name']).strip("[]'")
			if not str(jobs[job_number][1]['passed']).strip("[]'"):
				flow.append(['', job])
			Pipeline2Diagram.arrange_stages(job, jobs, flow)
		return flow


	# Set attributes for graphviz
	@staticmethod
	def set_graph_configuration(args):
		graph = False
		if (args.dot or args.png) and "pygraphviz" in sys.modules:
			graph = pgv.AGraph()
			graph.graph_attr['warnings'] = False
			graph.graph_attr['rankdir'] = 'LR'
			graph.graph_attr['bgcolor'] = '#3D3C3C'
			graph.graph_attr['pad'] = '0.8'
			graph.node_attr['style'] = 'filled'
			graph.node_attr['color'] = '#00C666'
			graph.node_attr['fillcolor'] = '#00C666'
			graph.node_attr['fontcolor'] = 'white'
			graph.node_attr['shape'] = 'box'
			graph.edge_attr['color'] = '#00C666'
			graph.node_attr['fontname'] = 'Ubuntu'
		return graph


	# Create graphic chart of jobs
	@staticmethod
	def flow_to_chart(flow, job, passed_jobs, recurs, graphic_args):
		recurs += 1
		for next_job in flow:
			if job == next_job[0]:
				Pipeline2Diagram.add_edge(passed_jobs, next_job, recurs, graphic_args)
				Pipeline2Diagram.flow_to_chart(flow, next_job[1], passed_jobs, recurs, graphic_args)


	# Add node to jobs list
	@staticmethod
	def add_node(passed_jobs, job, graphic_args):
		passed_jobs.append(job)
		print(" -", job)
		args = graphic_args[0]
		graph = graphic_args[1]
		if (args.dot or args.png) and "pygraphviz" in sys.modules:
			graph.add_node(job)


	# Add edge to jobs list
	@staticmethod
	def add_edge(passed_jobs, next_job, recurs, graphic_args):
		passed_jobs.append(next_job[1])
		space = recurs * "  "
		print(space, " - ", next_job[1])
		args = graphic_args[0]
		graph = graphic_args[1]
		if (args.dot or args.png) and "pygraphviz" in sys.modules:
			graph.add_edge(next_job[0], next_job[1])


# Main
def main():
	diag = Pipeline2Diagram()
	args = diag.parse_arguments()
	graph = diag.set_graph_configuration(args)
	jobs = diag.get_plan(diag.read_yaml(args), 'passed')
	flow = []
	diag.list_jobs(jobs, flow)

	# Compose flow chart
	passed_jobs = []
	for job in flow:
		recurs = 0
		if job[1] not in passed_jobs:
			graphic_args = [args, graph]
			diag.add_node(passed_jobs, job[1], graphic_args)
			diag.flow_to_chart(flow, job[1], passed_jobs, recurs, graphic_args)

	# Create .dot and .png files
	if (args.dot or args.png) and "pygraphviz" in sys.modules:
		graph.layout(prog='dot')
		if args.dot:
			if not '.dot' in args.dot.lower():
				save_name = args.dot+'.dot'
			else:
				save_name = args.dot
			graph.write(save_name)
		if args.png:
			if not '.png' in args.png.lower():
				save_name = args.png+'.png'
			else:
				save_name = args.png
			graph.draw(save_name)


if __name__ == '__main__':
	main()
