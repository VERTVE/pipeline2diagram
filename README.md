
# "Concourse pipeline yaml to diagram"

A Python script to easily visualize a Concourse pipeline yaml file.

Version 0.5

Lists pipeline workflow (jobs) to terminal, and optionally to .dot and .png files.

## Usage

```bash
usage: pipeline2diagram.py [-h] [-p PIPELINE] [-d DOT] [-g PNG]

Visualize Concourse pipeline yaml files.

optional arguments:
  -h, --help            show this help message and exit
  -p PIPELINE, --pipeline PIPELINE
                        pipeline yaml to load
  -d DOT, --dot DOT     dot file name
  -g PNG, --png PNG     png file name

```

## Output

```bash
$ python pipeline2diagram.py -g flowchart

Pipeline: pipeline.yml

 - clamav
 - decompile-binary
 - binwalk
    -  ioc_strings
      -  feature_extractor
 - peframe
 - ssdeep
 - ssdc
```

### Output .dot / .png image

![flowchart](flowchart.png)
